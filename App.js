/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import 'react-native-gesture-handler'
import { StyleSheet, View, StatusBar } from 'react-native';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import LoadingScreen from './src/screens/LoadingScreen';
import LoginScreen from './src/screens/LoginScreen';
import SignUpScreen from './src/screens/SignUpScreen';
import MainScreen from './src/screens/MainScreen';

const App: () => React$Node = () => {
  return (
    <>
      <View style={styles.container}>
        <StatusBar backgroundColor='#F7A50C' />
        <MenuStart />
      </View>
    </>
  );
};

const MenuStart = createAppContainer(createSwitchNavigator(
  {
    Loading: LoadingScreen,
    Login: LoginScreen,
    SignUp: SignUpScreen,
    Main: MainScreen,
  },
  {
    initialRouteName: 'Login'
  }
));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;
