/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

// import firebase from 'react-native-firebase';

import HomeScreen from './Main/HomeScreen'
import ChatScreen from './Main/ChatScreen'
import SettingsScreen from './Main/SettingsScreen'
import AddTask from './Main/AddTask'

export default class MainScreen extends React.Component {
  state = { currentUser: null }
  // componentDidMount() {
  //   const { currentUser } = firebase.auth()
  //   this.setState({ currentUser })
  // }

  render() {
    const { currentUser } = this.state
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.titlePage}>Votre programme</Text>
          {/* <Button title="Deconnexion" onPress={() => firebase.auth().signOut().then(function() {}).catch(function(error) {})} /> */}
        </View>
        <Menu />
      </View>
    );
  }
}

const HomeStack = createStackNavigator({
  Home: {
    screen: HomeScreen, 
    navigationOptions: {
        header: null,
    },
  },
  AddTask: AddTask,
});

const TabNavigator = createMaterialTopTabNavigator(
  {
    Home: HomeStack,
    Chat: ChatScreen,
    Settings: SettingsScreen,
  },
  {
    tabBarOptions: {
      activeTintColor: '#F5FCFF',
      inactiveTintColor: 'gray',
      showLabel: true,
      tabStyle: {
        borderWidth:0.2,
        borderColor:'grey'
      },
      style: {
        backgroundColor: 'none',
      },
      indicatorStyle: {
        height: '100%',
        backgroundColor: '#F7A50C',
      },
    },
    tabBarPosition: 'bottom',
  }
);

const Menu = createAppContainer(TabNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    
  },
  headerContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    height: hp('8%'),
    width: wp('100%'),
    paddingLeft: wp('5%'),
    paddingRight: wp('5%'),
    backgroundColor: '#F7A50C',
    flexDirection: 'row',
  },
  titlePage: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'white',
  },
});