/* eslint-disable prettier/prettier */
import React from 'react'
import { StyleSheet, Text, TextInput, View, TouchableOpacity, Button } from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class SignUpScreen extends React.Component {
  state = { username: '', email: '', password: '', password2: '', errorMessage: null }
  // handleSignUp = () => {
  //   let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
  //   if(this.state.email === '' || this.state.password === ''){
  //     this.setState({ errorMessage: 'Merci de remplir tout les inputs' })
  //   } else {
  //     if(reg.test(this.state.email) === false){
  //       this.setState({ errorMessage: 'Mail non valide' })
  //     } else {
        
  //         this.setState({ errorMessage: null })
  //         firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
  //           .then((userCredentials)=>{
  //             this.props.navigation.navigate('Main');
  //           })
  //           .catch(function(error) {
  //             this.setState({ errorMessage: error.message });
  //           });
        
  //     }
  //   }
  // }
render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.titlePage}>Build Body</Text>
        </View>
        <View style={styles.SingUpContainer}>
          <View>
            {this.state.successMessage && <Text style={{ color: 'green' }}>{this.state.successMessage}</Text>}
            {this.state.errorMessage && <Text style={{ color: 'red' }}>{this.state.errorMessage}</Text>}
            <TextInput
              placeholder="Email"
              autoCapitalize="none"
              style={styles.textInput}
              onChangeText={email => this.setState({ email })}
              value={this.state.email}
            />
            <TextInput
              secureTextEntry
              placeholder="Mot de Passe"
              autoCapitalize="none"
              style={styles.textInput}
              onChangeText={password => this.setState({ password })}
              value={this.state.password}
            />
            <TouchableOpacity style={styles.btnSignUp} onPress={this.handleSignUp}>
              <Text style={styles.btnText}> Connexion </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('SignUp')}>
            <Text style={styles.btnText}> Besoin d'un compte ? Inscription </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  headerContainer: {
    justifyContent: 'center',
    height: hp('30%'),
  },
  titlePage: {
    fontSize: 40,
    fontWeight: 'bold',
  },
  SingUpContainer: {
    justifyContent: 'space-between',
    height: hp('60%'),
    marginLeft:wp('2%'),
    marginRight:wp('2%'),
    width:wp('96%'),
  },
  textInput: {
    height: 40,
    width: '100%',
    borderColor: 'gray',
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderBottomLeftRadius: 10,
    paddingLeft:10,
    marginTop: 8
  },
  InputTwoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textInputTwo: {
    height: 40,
    width: '48%',
    borderColor: 'gray',
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderBottomLeftRadius: 10,
    paddingLeft:10,
    marginTop: 8
  },
  btnSignUp: {
    marginTop: 18,
    alignItems: 'center',
    backgroundColor: '#F7A50C',
    padding: 10
  },
  btn: {
    marginTop: 8,
    alignItems: 'center',
    backgroundColor: '#F7A50C',
    padding: 10
  },
  btnText: {
    color: 'white',
  }
})