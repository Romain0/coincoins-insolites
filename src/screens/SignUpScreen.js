/* eslint-disable prettier/prettier */
import React from 'react'
import { StyleSheet, Text, TextInput, View, TouchableOpacity, Button } from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
// import firebase from 'react-native-firebase'

export default class SignUpScreen extends React.Component {
  state = { username: '', email: '', password: '', password2: '', errorMessage: null, successMessage: null }
  // handleSignUp = () => {
  //   let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
  //   if((this.state.email === '' || this.state.password === '') || (this.state.email === '' && this.state.password === '')){
  //     this.setState({ errorMessage: 'Merci de remplir tout les inputs' })
  //   } else {
  //     if(reg.test(this.state.email) === false){
  //       this.setState({ errorMessage: 'Mail non valide' })
  //     } else {
  //       if(this.state.password === this.state.password2){
  //         this.setState({ errorMessage: null })
  //         firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
  //           .then((userCredentials)=>{
  //               if(userCredentials.user){
  //                 userCredentials.user.updateProfile({
  //                   displayName: this.state.username
  //                 }).then((s)=> {
  //                   this.props.navigation.navigate('Main');
  //                 })
  //               }
  //           })
  //           .catch(function(error) {
  //             this.setState({ errorMessage: error.message });
  //           });
  //       } else {
  //         this.setState({ errorMessage: 'Les mots de passe doivent être identique' })
  //       }
  //     }
  //   }
  // }
render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.titlePage}>Build Body</Text>
        </View>
        <View style={styles.SingUpContainer}>
          <View>
            {this.state.successMessage && <Text style={{ color: 'green' }}>{this.state.successMessage}</Text>}
            {this.state.errorMessage && <Text style={{ color: 'red' }}>{this.state.errorMessage}</Text>}
            <TextInput
              placeholder="Email"
              autoCapitalize="none"
              style={styles.textInput}
              onChangeText={email => this.setState({ email })}
              value={this.state.email}
            />
            <View style={styles.InputTwoContainer}>
              <TextInput
                secureTextEntry
                placeholder="Mot de Passe"
                autoCapitalize="none"
                style={styles.textInputTwo}
                onChangeText={password => this.setState({ password })}
                value={this.state.password}
              />
              <TextInput
                secureTextEntry
                placeholder="Retapper le mot de passe"
                autoCapitalize="none"
                style={styles.textInputTwo}
                onChangeText={password2 => this.setState({ password2 })}
                value={this.state.password2}
              />
            </View>
            <TextInput
              placeholder="Nom"
              autoCapitalize="none"
              style={styles.textInput}
              onChangeText={username => this.setState({ username })}
              value={this.state.username}
            />
            <TouchableOpacity style={styles.btnSignUp} onPress={this.handleSignUp}>
              <Text style={styles.btnText}> Inscription </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.btn} onPress={() => this.props.navigation.navigate('Login')}>
            <Text style={styles.btnText}> Vous avez déjà un compte ? Connexion </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  headerContainer: {
    justifyContent: 'center',
    height: hp('30%'),
  },
  titlePage: {
    fontSize: 40,
    fontWeight: 'bold',
  },
  SingUpContainer: {
    justifyContent: 'space-between',
    height: hp('60%'),
    marginLeft:wp('2%'),
    marginRight:wp('2%'),
    width:wp('96%'),
  },
  textInput: {
    height: 40,
    width: '100%',
    borderColor: 'gray',
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderBottomLeftRadius: 10,
    paddingLeft:10,
    marginTop: 8
  },
  InputTwoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textInputTwo: {
    height: 40,
    width: '48%',
    borderColor: 'gray',
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderBottomLeftRadius: 10,
    paddingLeft:10,
    marginTop: 8
  },
  btnSignUp: {
    marginTop: 18,
    alignItems: 'center',
    backgroundColor: '#F7A50C',
    padding: 10
  },
  btn: {
    marginTop: 8,
    alignItems: 'center',
    backgroundColor: '#F7A50C',
    padding: 10
  },
  btnText: {
    color: 'white',
  }
})