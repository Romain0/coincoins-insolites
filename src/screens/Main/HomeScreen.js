/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import ItemComponent from '../../components/ItemComponent';
// import firebase from 'react-native-firebase';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
	}
});

export default class HomeScreen extends Component {
	state = {
		items: [],
		users: [],
    currentUser: null
	};

// 	componentDidMount() {
//     const { currentUser } = firebase.auth()
//     this.setState({ currentUser })
//     let itemsRef = firebase.database().ref('/programmes/' + currentUser.uid);
// 		itemsRef.on('value', snapshot => {
// 			let data = snapshot.val();
// 			let items = Object.values(data);
// 			this.setState({ items });
// 		}, function(error){
//       console.log(error);
// 		});
	
// 		let userRef = firebase.database().ref('users');
// 			userRef.once('value', snapshot => {
// 				let data = snapshot.val();
// 				// let users = Object.values(data);
// 				console.log(data);
// 				// this.setState({ users });
// 			}, function(error){
//   	    console.log(error);
// 		});
// }

	render() {
    console.log(this.state.items)
		return (
			<View style={styles.container}>
				{this.state.items.length > 0 ? (
					<ItemComponent items={this.state.items} />
				) : (
					<Text>No items</Text>
        )}
				<Button title="Add Tasks" onPress={() => this.props.navigation.navigate('AddTask')} />
			</View>
		);
	}
}
