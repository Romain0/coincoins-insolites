/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
//import moment from 'moment'

//moment.locale('fr')

export default class ItemComponent extends Component {
	static propTypes = {
		items: PropTypes.array.isRequired
	};

	render() {
		return (
			<View style={styles.itemsList}>
				{this.props.items.map((item, index) => {
					return (
						<View style={styles.itemsContainer} key={index}>
							<Text style={styles.itemtext}>{item.nom}</Text>
                            <Text style={styles.itemtextMin}>{item.série} x {item.répétitions}</Text>
                            <Text style={styles.itemtextMin}>{item.pause} min de pause</Text>
                            {/* <Text style={styles.itemtextRightTop}>A faire le {moment(item.date).locale("fr").format("DD MMMM YYYY")}</Text> */}
                            <Text style={styles.itemtextStatus}>{item.statut}</Text>
						</View>
					);
				})}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	itemsList: {
		flex: 1,
    },
    itemsContainer: {
        borderWidth: 1,
        paddingLeft:wp('1%'),
        paddingRight:wp('1%'),
        paddingTop:wp('1%'),
        paddingBottom:wp('1%'),
        marginTop:wp('2%'),
        marginLeft:wp('2%'),
        marginRight:wp('2%'),
    },
	itemtext: {
		fontSize: 24,
        fontWeight: 'bold',
    },
    itemtextMin: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    itemtextRightTop: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'right',
        marginTop: 10,
    },
    itemtextStatus: {
        fontSize: 16,
        fontWeight: 'bold',
        alignSelf: 'flex-end',
        backgroundColor: 'green',
        color: 'white',
        paddingLeft:5,
        paddingRight:5,
        paddingTop:5,
        paddingBottom:5,
        borderRadius:10,
        width: 'auto'
    }
});
